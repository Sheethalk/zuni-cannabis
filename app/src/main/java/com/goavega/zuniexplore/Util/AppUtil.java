package com.goavega.zuniexplore.Util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.json.JSONException;
import org.json.JSONObject;

public class AppUtil {
	
	public static int CURRENT_NETWORK = ConnectivityManager.TYPE_WIFI;
	public static String NETORK_FAILURE_REASON = null;

	public static void navigateToScreen(Activity parentactivity, String activityName){
			
			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.setClassName(AppConstants.APPLICATION_PACKAGE_NAME,activityName);
			parentactivity.startActivity(openActivity);
	
		}
	
	public static boolean isInternetAvailable(Context context) {
		ConnectivityManager conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		final android.net.NetworkInfo wifi = conMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		final android.net.NetworkInfo mobile = conMgr
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		NETORK_FAILURE_REASON = null;
		if (wifi!=null && (wifi.getState() == NetworkInfo.State.CONNECTED
				|| wifi.getState() == NetworkInfo.State.CONNECTING)) {
			CURRENT_NETWORK = ConnectivityManager.TYPE_WIFI;
			return true;

		} else if (mobile!=null && (mobile.getState() == NetworkInfo.State.CONNECTED
				|| mobile.getState() == NetworkInfo.State.CONNECTING)) {
			CURRENT_NETWORK = ConnectivityManager.TYPE_MOBILE;
			return true;
		} else if ((conMgr.getNetworkInfo(0)!=null && conMgr.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED)
				|| (conMgr.getNetworkInfo(1)!=null && conMgr.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED)) {
			return true;

		} else if ((conMgr.getNetworkInfo(0)!=null && conMgr.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING)
				|| (conMgr.getNetworkInfo(1)!=null && conMgr.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING)) {
			return true;

		} else if (mobile!=null && (mobile.getState() == NetworkInfo.State.DISCONNECTED
				|| mobile.getState() == NetworkInfo.State.DISCONNECTING)) {

			NETORK_FAILURE_REASON = mobile.getReason();
			CURRENT_NETWORK = ConnectivityManager.TYPE_MOBILE;
		} else if (wifi!=null &&  (wifi.getState() == NetworkInfo.State.DISCONNECTED
				|| wifi.getState() == NetworkInfo.State.DISCONNECTING)) {
			CURRENT_NETWORK = ConnectivityManager.TYPE_WIFI;
			NETORK_FAILURE_REASON = wifi.getReason();

		} else if (mobile!=null && (mobile.getState() == NetworkInfo.State.DISCONNECTED
				|| mobile.getState() == NetworkInfo.State.DISCONNECTING)) {

			NETORK_FAILURE_REASON = mobile.getReason();
			CURRENT_NETWORK = ConnectivityManager.TYPE_MOBILE;
		} else if (wifi!=null &&  (wifi.getState() == NetworkInfo.State.DISCONNECTED
				|| wifi.getState() == NetworkInfo.State.DISCONNECTING)) {
			CURRENT_NETWORK = ConnectivityManager.TYPE_WIFI;
			NETORK_FAILURE_REASON = wifi.getReason();

		}
		return false;
	}
	
	
	public static void navigateToHomeScreen(Activity parentactivity){
		
		Intent openActivity = new Intent();
		openActivity.setAction(Intent.ACTION_VIEW);
		openActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		openActivity.setClassName("com.goavega.zuniexplore",
				"com.goavega.zuniexplore.View.HomeActivity");
		parentactivity.startActivity(openActivity);
		parentactivity.finish();
		
	}
	
	
	public static boolean ParseJson(String jsonStr) {

			try {
				
				JSONObject jObject = new JSONObject(jsonStr);
				boolean response = jObject.getBoolean("success");
				return response;
				
			} catch (JSONException e) {
				 
				e.printStackTrace();
				return false;
			
			}
 
	}
	
	
}
