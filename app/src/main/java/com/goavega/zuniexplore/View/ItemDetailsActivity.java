package com.goavega.zuniexplore.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.goavega.zuniexplore.Adapters.ItemDetailsAdapter;
import com.goavega.zuniexplore.Model.Items;
import com.goavega.zuniexplore.Network.ItemServiceApi;
import com.goavega.zuniexplore.R;

public class ItemDetailsActivity extends AppCompatActivity {
    TextView typeText;
    Items singleItem[];
    RecyclerView mRecyclerView;
    ImageView searchIcon;
    ItemDetailsAdapter itemDetailsAdapter;
    EditText searchItemText;
    String searchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        typeText=(TextView) findViewById(R.id.typText);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchItemText=(EditText)findViewById(R.id.searchItemText);
        String itemType=getIntent().getStringExtra("itemType");
        typeText.setText(itemType);
        singleItem=new ItemServiceApi(ItemDetailsActivity.this).getSingleItem(itemType);
        itemDetailsAdapter = new ItemDetailsAdapter(singleItem);
        mRecyclerView.setAdapter(itemDetailsAdapter);

        searchIcon=(ImageView)findViewById(R.id.searchIcon);
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchItem=searchItemText.getText().toString();
                if(!searchItem.equals("")) {
                    Intent i = new Intent(ItemDetailsActivity.this, SearchItemActivity.class);
                    i.putExtra("searchItemValue", searchItem);
                    startActivity(i);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Enter Search Item value", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
