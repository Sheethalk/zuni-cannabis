package com.goavega.zuniexplore.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import com.goavega.zuniexplore.Adapters.SearchItemAdapter;
import com.goavega.zuniexplore.Model.Items;
import com.goavega.zuniexplore.Network.ItemServiceApi;
import com.goavega.zuniexplore.R;
import java.util.ArrayList;

public class SearchItemActivity extends AppCompatActivity {
    TextView typeText;
    RecyclerView mRecyclerView;
    SearchItemAdapter itemDetailsAdapter;
    ArrayList<Items> filterItems=new ArrayList<Items>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_item);
        typeText=(TextView) findViewById(R.id.typText);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        String searchItemValue=getIntent().getStringExtra("searchItemValue");
        typeText.setText(getString(R.string.search_result)+" "+searchItemValue);

        filterItems=new ItemServiceApi(SearchItemActivity.this).filterItems(searchItemValue);
        itemDetailsAdapter = new SearchItemAdapter(filterItems);
        mRecyclerView.setAdapter(itemDetailsAdapter);
    }
}
