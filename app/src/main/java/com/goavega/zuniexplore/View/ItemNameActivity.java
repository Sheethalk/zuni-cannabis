package com.goavega.zuniexplore.View;

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import com.goavega.zuniexplore.Adapters.ItemNameAdapter;
import com.goavega.zuniexplore.Model.ItemModel;
import com.goavega.zuniexplore.Network.ItemServiceApi;
import com.goavega.zuniexplore.R;
import java.util.ArrayList;

public class ItemNameActivity extends AppCompatActivity {

    ImageView searchIconView;
    ArrayList<ItemModel> itemList=new ArrayList<ItemModel>();
    ArrayList<String> itemNameList=new ArrayList<String>();
    RecyclerView mRecyclerView;
    ItemNameAdapter adapter;
    EditText searchItemText;
    String searchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_names);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        searchItemText=(EditText)findViewById(R.id.searchItemText);
        itemList=new ItemServiceApi(ItemNameActivity.this).getAllItemDetails();
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchIconView=(ImageView)findViewById(R.id.searchItemImage);


        for(int k=0;k<itemList.size();k++)
        {
            itemNameList.add(itemList.get(k).getType());
        }
        adapter = new ItemNameAdapter(itemNameList);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Intent i = new Intent(ItemNameActivity.this, ItemDetailsActivity.class);
                i.putExtra("itemType", itemNameList.get(position));
                startActivity(i);

            }
        }));



        searchIconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchItem=searchItemText.getText().toString();
                Intent i=new Intent(ItemNameActivity.this,SearchItemActivity.class);
                i.putExtra("searchItemValue",searchItem);
                startActivity(i);
            }
        });

    }

    private interface ClickListener {
        void onClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        final private GestureDetector gestureDetector;
        final private ItemNameActivity.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ItemNameActivity.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

}

