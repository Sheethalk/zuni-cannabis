package com.goavega.zuniexplore.View;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;


import com.goavega.zuniexplore.Util.AppContext;
import com.goavega.zuniexplore.R;
import com.goavega.zuniexplore.Model.Answer;
import com.goavega.zuniexplore.Util.AppUtil;
import com.goavega.zuniexplore.Model.Product;


//import com.android.wildfire.network.HttpAsyncTask;

import com.goavega.zuniexplore.Network.ServiceListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Result extends Activity implements ServiceListener {
	
	private AppContext appContext;
	private int whiteCount,yellowCount,orangeCount,blueCount = 0;
	private ArrayList<Integer> flameCountList;
	
	private TextView product_title_1, product_info_1, product_description_1,restart_tv,product_title_2, product_info_2, product_description_2;
	private ImageView restart_Img;
	private JSONObject userResponseObject;
	
	//v2.0
	private View colorBar1,colorBar2;
	private ImageView productImg1, productImg2;
	
	private Integer recommendProd1,recommendProd2 = 0;
	private TextView recommendText1, recommendText2;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
//		setContentView(R.layout.final_result);
		
		setContentView(R.layout.result);  // v2.0
		
		whiteCount = yellowCount = orangeCount = blueCount = 0;
		
		product_title_1 = (TextView)findViewById(R.id.recommend_prod1_tv);
		product_description_1 = (TextView)findViewById(R.id.recommend_prod1_desc_tv);
		product_info_1 = (TextView)findViewById(R.id.recommend_prod1_info_tv);
		
		product_title_2 = (TextView)findViewById(R.id.recommend_prod2_tv);
		product_description_2 = (TextView)findViewById(R.id.recommend_prod2_desc_tv);
		product_info_2 = (TextView)findViewById(R.id.recommend_prod2_info_tv);
		
		//v2.0
		productImg1 = (ImageView)findViewById(R.id.product1_logo);
		productImg2 = (ImageView)findViewById(R.id.product2_logo);
		
		//recommendText1 = (TextView)findViewById(R.id.recommendation_txt1);
		//recommendText2 = (TextView)findViewById(R.id.recommendation_txt2);
		
		//colorBar1 = findViewById(R.id.prod1_bar);
		//colorBar2 = findViewById(R.id.prod2_bar);
	
		restart_tv =(TextView)findViewById(R.id.restart_tv);
		restart_tv.setOnClickListener(onRestartClickListener);
		
		//restart_Img = (ImageView)findViewById(R.id.back);
		//restart_Img.setOnClickListener(onRestartClickListener);
		
		appContext = (AppContext)getApplication();

		flameCountList = new ArrayList<Integer>();
		
		userResponseObject = new JSONObject();
		try {
			
			userResponseObject.put("email", appContext.getUserObject().getEmailId());
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		//
		JSONArray answersArrayJson = new JSONArray();
		
		if(appContext.getAnswerListObject()!= null){
			 
	
			if(appContext.getAnswerListObject().size() > 0){
				
				for(Answer ans: appContext.getAnswerListObject()){
					System.out.println("Que ::- "+ ans.getQuestionNo());
					System.out.println("Option ::- "+ ans.getOptionNo());
					
					JSONObject answerJSONObject = new JSONObject();
					try {
						
						if(null != ans.getQuestionNo()){
							answerJSONObject.put("key", Integer.parseInt(ans.getQuestionNo()));
						}
						if(null != ans.getOptionNo()){
							answerJSONObject.put("value", Integer.parseInt(ans.getOptionNo()));
						}
						answersArrayJson.put(answerJSONObject);
						
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					
					if(ans.getProductList()!=null && ans.getProductList().size()>0 ){
						for(Product prod: ans.getProductList()){
							System.out.println("Prod ::: "+ prod.getName() + "Val :- "+prod.getValue());
								
								if(prod.getName().equals(appContext.getCategoryListObject().get(0))){
									whiteCount+= prod.getValue(); 
									
								}else if(prod.getName().equals(appContext.getCategoryListObject().get(1))){
									yellowCount+= prod.getValue();
								}
								else if(prod.getName().equals(appContext.getCategoryListObject().get(2))){
									orangeCount+= prod.getValue();
								}
								else {
									blueCount+= prod.getValue();
								}
						}
					}
					
				}
			}
		} 		
		
		
		//creating JSON request 
		
		try {
		
			userResponseObject.put("answers", answersArrayJson);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println("Request JSON : "+ userResponseObject);
		
		// post to server
		
/*
		HttpAsyncTask httpAsyncTask = new HttpAsyncTask(Result.this, userResponseObject, Result.this);
		httpAsyncTask.execute(AppConstants.SERVER_END_POINT + AppConstants.POST_USER_ANSWERS_API);
*/

		
		// Calculation logic for results
		
		flameCountList.add(whiteCount);
		flameCountList.add(yellowCount);
		flameCountList.add(orangeCount);
		flameCountList.add(blueCount);
		
		System.out.println("whiteCount :"+whiteCount);
		System.out.println("yellowCount :"+yellowCount);
		System.out.println("orangeCount :"+orangeCount);
		System.out.println("blueCount :"+blueCount);
		
		
		int prodId = findMaxIndex(flameCountList);
		
		//v1.0
		/*switch (prodId) {
			case 0:
					product_title.setText(getResources().getString(R.string.white_prod_title));
					//product_info.setText(getResources().getString(R.string.white_prod_info));
					product_description.setText(getResources().getString(R.string.white_prod_desc));
					break;
				
			case 1:
					product_title.setText(getResources().getString(R.string.yellow_prod_title));
					//product_info.setText(getResources().getString(R.string.yellow_prod_info));
					product_description.setText(getResources().getString(R.string.yellow_prod_desc));
					break;
				
			case 2:
					product_title.setText(getResources().getString(R.string.orange_prod_title));
					//product_info.setText(getResources().getString(R.string.orange_prod_info));
					product_description.setText(getResources().getString(R.string.orange_prod_desc));
					break;
				
			case 3:
					product_title.setText(getResources().getString(R.string.blue_prod_title));
					//product_info.setText(getResources().getString(R.string.blue_prod_info));
					product_description.setText(getResources().getString(R.string.blue_prod_desc));
					break;
	
			default:
				break;
		}*/
		
		
		//v2.0
		
		findTop2Results(flameCountList);
		
		//For prod 1
		
		Resources res = getResources();
		
		switch (prodId) {
			case 0:
					product_title_1.setText(res.getString(R.string.white_title));
					product_info_1.setText(res.getString(R.string.white_info));
					product_description_1.setText(res.getString(R.string.white_desc));
					productImg1.setImageDrawable(res.getDrawable(R.drawable.white));
					/*recommendText1.setText(String.format(res.getString(R.string.recommendation_txt1),
							res.getString(R.string.white_title), "WHITE"));*/
					colorBar1.setBackgroundColor(res.getColor(R.color.white));
					break;
				
			case 1:
					product_title_1.setText(res.getString(R.string.yellow_title));
					product_info_1.setText(res.getString(R.string.yellow_info));
					product_description_1.setText(res.getString(R.string.yellow_desc));
					productImg1.setImageDrawable(res.getDrawable(R.drawable.yellow));
					/*recommendText1.setText(String.format(res.getString(R.string.recommendation_txt1),
							res.getString(R.string.yellow_title), "YELLOW"));
					/colorBar1.setBackgroundColor(res.getColor(R.color.yellow));*/
					break;
				
			case 2:
					product_title_1.setText(res.getString(R.string.orange_title));
					product_info_1.setText(res.getString(R.string.orange_info));
					product_description_1.setText(res.getString(R.string.orange_desc));
					productImg1.setImageDrawable(res.getDrawable(R.drawable.orange));
					/*recommendText1.setText(String.format(res.getString(R.string.recommendation_txt1),
							res.getString(R.string.orange_title), "ORANGE"));
					colorBar1.setBackgroundColor(res.getColor(R.color.orange));*/
					break;
				
			case 3:
					product_title_1.setText(res.getString(R.string.blue_title));
					product_info_1.setText(res.getString(R.string.blue_info));
					product_description_1.setText(res.getString(R.string.blue_desc));
					productImg1.setImageDrawable(res.getDrawable(R.drawable.blue));
					/*recommendText1.setText(String.format(res.getString(R.string.recommendation_txt1),
							res.getString(R.string.blue_title), "BLUE"));
					colorBar1.setBackgroundColor(res.getColor(R.color.blue));*/
					break;
	
			default:
				break;
		}
		
		
		//For prod 2
		
				switch (recommendProd2) {
					case 0:
							product_title_2.setText(res.getString(R.string.white_title));
							product_info_2.setText(res.getString(R.string.white_info));
							product_description_2.setText(res.getString(R.string.white_desc));
							productImg2.setImageDrawable(res.getDrawable(R.drawable.white));
							/*recommendText2.setText(String.format(res.getString(R.string.recommendation_txt2),
									res.getString(R.string.white_title), "WHITE"));
							colorBar2.setBackgroundColor(res.getColor(R.color.white));*/
							break;
						
					case 1:
							product_title_2.setText(res.getString(R.string.yellow_title));
							product_info_2.setText(res.getString(R.string.yellow_info));
							product_description_2.setText(res.getString(R.string.yellow_desc));
							productImg2.setImageDrawable(res.getDrawable(R.drawable.yellow));
							/*recommendText2.setText(String.format(res.getString(R.string.recommendation_txt2),
									res.getString(R.string.yellow_title), "YELLOW"));
							colorBar2.setBackgroundColor(res.getColor(R.color.yellow));*/
							break;
						
					case 2:
							product_title_2.setText(res.getString(R.string.orange_title));
							product_info_2.setText(res.getString(R.string.orange_info));
							product_description_2.setText(res.getString(R.string.orange_desc));
							productImg2.setImageDrawable(res.getDrawable(R.drawable.orange));
							/*recommendText2.setText(String.format(res.getString(R.string.recommendation_txt2),
									res.getString(R.string.orange_title), "ORANGE"));
							colorBar2.setBackgroundColor(res.getColor(R.color.orange));*/
							break;
						
					case 3:
							product_title_2.setText(res.getString(R.string.blue_title));
							product_info_2.setText(res.getString(R.string.blue_info));
							product_description_2.setText(res.getString(R.string.blue_desc));
							productImg2.setImageDrawable(res.getDrawable(R.drawable.blue));
							/*recommendText2.setText(String.format(res.getString(R.string.recommendation_txt2),
									res.getString(R.string.blue_title), "BLUE"));
							colorBar2.setBackgroundColor(res.getColor(R.color.blue)); */
							break;
			
					default:
						break;
				}
		
	}
	
	
	private Integer findMaxIndex(ArrayList<Integer> flameCountList) {
		  Integer max = -1;
		  int pos = 0;
		  
		   for(int index=0; index < flameCountList.size(); index++){
			   if(flameCountList.get(index) > max){
				   max = flameCountList.get(index);
				   pos = index;
			   }	   
		   }
		   
		   return pos;
	}

	private void findTop2Results(ArrayList<Integer> flameCountList){
		
		int secondlargest = Integer.MIN_VALUE;
        int largest = Integer.MIN_VALUE;
        
        for(int index=0; index < flameCountList.size(); index++){
            if (largest < flameCountList.get(index)) {
                secondlargest = largest;
                largest = flameCountList.get(index);
                recommendProd1 = index;
            }
            if (secondlargest < flameCountList.get(index) && largest != flameCountList.get(index))
                secondlargest = flameCountList.get(index);
            	recommendProd2 = index;
        }
	}

	@Override
	public void onServiceComplete(String posts) {

		boolean serverResponse = AppUtil.ParseJson(posts);
		
		String respString = (serverResponse)? "Success" : "Failure";
		
		System.out.println("Server Response : "+ respString);
		
	}
	
	
	protected OnClickListener onRestartClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			AppUtil.navigateToHomeScreen(Result.this);
			flameCountList = new ArrayList<Integer>();
			whiteCount = yellowCount = orangeCount = blueCount = 0;
			appContext.setAnswerListObject();
		}
		
		
	};
	

}
