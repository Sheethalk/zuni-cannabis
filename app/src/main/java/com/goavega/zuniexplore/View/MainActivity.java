package com.goavega.zuniexplore.View;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.goavega.zuniexplore.View.Question1;

import com.goavega.zuniexplore.R;
import com.goavega.zuniexplore.Util.PrefUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//import com.android.wildfire.ui.DiscoverTerpenailty;

public class MainActivity extends Activity {

	 private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP));
	    private Button startQuizButton,logoutBtn;
	    private ImageView logo;
	    private LinearLayout linearLayoutSplash ;
	    
	    private String userPwd = null;
	    

	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
	        setContentView(R.layout.splash_screen_layout);

	       /* linearLayoutSplash = (LinearLayout)findViewById(R.id.splash_layout);
	        linearLayoutSplash.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent startIntent = new Intent(MainActivity.this,DiscoverTerpenailty.class);
					startActivity(startIntent);
				}
			});
	        linearLayoutSplash.setOnTouchListener(new View.OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					switch(event.getAction()) {
		            case MotionEvent.ACTION_DOWN:
		            	Intent startIntent = new Intent(MainActivity.this,DiscoverTerpenailty.class);
						startActivity(startIntent);
		                return true;  
		        }
					return false;
				}
			});*/
	        
	        
	        // every time someone enters the kiosk mode, set the flag true
	        PrefUtils.setKioskModeActive(true, getApplicationContext());

	        startQuizButton = (Button)findViewById(R.id.start_quiz_btn);
	        startQuizButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					 Intent startIntent = new Intent(MainActivity.this,Question1.class);
					 startActivity(startIntent);
				}
			});
	        
	        
	       /* logo = (ImageView)findViewById(R.id.wildfire_logo);
	        logo.setOnClickListener(new View.OnClickListener() {
	            @Override
	            public void onClick(View v) {
	                // Break out!
//	                PrefUtils.setKioskModeActive(false, getApplicationContext());
	                showInputDialog();
	            }
	        });*/
	        
	        logoutBtn = (Button)findViewById(R.id.admin_logout_btn);
	        logoutBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					showInputDialog();
				}
			});
	        
	    }

	    @Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if(!hasFocus) {
			// Close every kind of system dialog
//	        	Toast.makeText(getApplicationContext(),"Close every kind of system dialog!", Toast.LENGTH_SHORT).show();
			Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
			sendBroadcast(closeDialog);
		}
	}

	@Override
	public void onBackPressed() {

	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (blockedKeys.contains(event.getKeyCode())) {
			return true;
		} else {
			return super.dispatchKeyEvent(event);
		}
	}
	    
	    protected void showInputDialog() {

			// get prompts.xml view
			LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
			View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
			alertDialogBuilder.setView(promptView);

			final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
			// setup a dialog window
			alertDialogBuilder.setCancelable(false)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							userPwd = editText.getText().toString().trim();
							if(null != userPwd){
								if(userPwd.equals("admin")){
									PrefUtils.setKioskModeActive(false, getApplicationContext());
									finish();
									System.exit(0);
								}
							}
						}
					})
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});

			// create an alert dialog
			AlertDialog alert = alertDialogBuilder.create();
			alert.show();
		}
}
