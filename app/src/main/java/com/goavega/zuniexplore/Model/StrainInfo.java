package com.goavega.zuniexplore.Model;

/**
 * Created by Administrator on 08-11-2016.
 */
public class StrainInfo {

    private String category;

    private String symbol;

    private String ratingCount;

    private String name;

    private String slug;

    private String rating;

    private String starImage;

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    public String getSymbol ()
    {
        return symbol;
    }

    public void setSymbol (String symbol)
    {
        this.symbol = symbol;
    }

    public String getRatingCount ()
    {
        return ratingCount;
    }

    public void setRatingCount (String ratingCount)
    {
        this.ratingCount = ratingCount;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getSlug ()
    {
        return slug;
    }

    public void setSlug (String slug)
    {
        this.slug = slug;
    }

    public String getRating ()
    {
        return rating;
    }

    public void setRating (String rating)
    {
        this.rating = rating;
    }

    public String getStarImage ()
    {
        return starImage;
    }

    public void setStarImage (String starImage)
    {
        this.starImage = starImage;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [category = "+category+", symbol = "+symbol+", ratingCount = "+ratingCount+", name = "+name+", slug = "+slug+", rating = "+rating+", starImage = "+starImage+"]";
    }

}
