package com.goavega.zuniexplore.Model;

/**
 * Created by Administrator on 08-11-2016.
 */
public class Items {

    private Pricing[] pricing;

    private String imagePath;

    private String description;

    private String name;

    private String addedOn;

    private String type;

    private String preOrderEnabled;

    private String key;

    public Pricing[] getPricing ()
    {
        return pricing;
    }

    public void setPricing (Pricing[] pricing)
    {
        this.pricing = pricing;
    }

    public String getImagePath ()
    {
        return imagePath;
    }

    public void setImagePath (String imagePath)
    {
        this.imagePath = imagePath;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getAddedOn ()
    {
        return addedOn;
    }

    public void setAddedOn (String addedOn)
    {
        this.addedOn = addedOn;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getPreOrderEnabled ()
    {
        return preOrderEnabled;
    }

    public void setPreOrderEnabled (String preOrderEnabled)
    {
        this.preOrderEnabled = preOrderEnabled;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [pricing = "+pricing+", imagePath = "+imagePath+", description = "+description+", name = "+name+", addedOn = "+addedOn+", type = "+type+", preOrderEnabled = "+preOrderEnabled+", key = "+key+"]";
    }

}
