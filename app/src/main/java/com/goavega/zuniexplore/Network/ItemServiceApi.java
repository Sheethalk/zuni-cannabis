package com.goavega.zuniexplore.Network;

import android.content.Context;
import com.goavega.zuniexplore.Model.ItemModel;
import  com.goavega.zuniexplore.Model.Items;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ItemServiceApi {
    final private Context context;
    public ItemServiceApi(Context context){
        this.context = context;
    }

    public ArrayList<ItemModel> getAllItemDetails()
    {
        URL url;
        ArrayList<ItemModel> itemList=new ArrayList<ItemModel>();
        StringBuilder sb =new StringBuilder();
        try {

            url = new URL("http://crimson.goavega.com/api/menu");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.addRequestProperty("x-client","3lpN2uNc8WhIMdMutSBIkkq8wrtB+Rovw399oxVbQck=");
            urlConnection.addRequestProperty("accept","text/json");
            int status = urlConnection.getResponseCode();
            if(status==201||status==200){
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                Gson gson = new Gson();
                ItemModel[] itemModel = gson.fromJson(sb.toString(), ItemModel[].class);
                itemList = new ArrayList<ItemModel>(Arrays.asList(itemModel));

            }
            urlConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return itemList;
    }


    public Items[] getSingleItem(String itemType)
    {
        Items singleItem[]=null;
        ArrayList<ItemModel> itemList=getAllItemDetails();

        for(int i=0;i<itemList.size();i++)
        {
            if(itemList.get(i).getType().equals(itemType))
            {
                singleItem=itemList.get(i).getItems();
            }
        }
        return  singleItem;
    }



    public ArrayList<Items> filterItems(String searchItemValue)
    {

        Items[] singleItem=null;
        ArrayList<Items[]> filterItems1=new ArrayList<>();
        ArrayList<Items> filterItems=new ArrayList<Items>();
        ArrayList<ItemModel> itemList=getAllItemDetails();
        for(int i=0;i<itemList.size();i++)
        {
            singleItem=itemList.get(i).getItems();
            filterItems1.add(singleItem);
        }
        for(int i=0;i<filterItems1.size();i++) {
            singleItem = filterItems1.get(i);

            for (int j = 0; j < singleItem.length; j++) {
                Pattern p= Pattern.compile("\\b"+searchItemValue.toLowerCase()+"\\b");
                Matcher m=p.matcher(singleItem[j].getName().toLowerCase());
                if(m.find())
                   filterItems.add(singleItem[j]);

            }
        }
        return  filterItems;
    }
}
