package com.goavega.zuniexplore.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.goavega.zuniexplore.Model.Items;
import com.goavega.zuniexplore.Model.Pricing;
import com.goavega.zuniexplore.R;

public class ItemDetailsAdapter extends RecyclerView.Adapter<ItemDetailsAdapter.MyViewHolder> {
    final private Items[] nameList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final public TextView nameText,
                descriptionText,
                price1Text,
                price2Text,
                price3Text,
                price4Text,
                price5Text,
                unit1Text,
                unit2Text,
                unit3Text,
                unit4Text,
                unit5Text;


        public MyViewHolder(View view) {
            super(view);
            nameText = (TextView) view.findViewById(R.id.titleText);
            descriptionText = (TextView) view.findViewById(R.id.descriptionText);
            price1Text=(TextView)view.findViewById(R.id.price1Text);
            price2Text=(TextView)view.findViewById(R.id.price2Text);
            price3Text=(TextView)view.findViewById(R.id.price3Text);
            price4Text=(TextView)view.findViewById(R.id.price4Text);
            price5Text=(TextView)view.findViewById(R.id.price5Text);
            unit1Text=(TextView)view.findViewById(R.id.unit1Text);
            unit2Text=(TextView)view.findViewById(R.id.unit2Text);
            unit3Text=(TextView)view.findViewById(R.id.unit3Text);
            unit4Text=(TextView)view.findViewById(R.id.unit4Text);
            unit5Text=(TextView)view.findViewById(R.id.unit5Text);
        }


    }

    public ItemDetailsAdapter(Items[] nameList) {
        this.nameList = nameList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_details_list, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(ItemDetailsAdapter.MyViewHolder holder, int position) {

        holder.nameText.setText(nameList[position].getName());
        holder.descriptionText.setText(nameList[position].getDescription());
        int length=nameList[position].getPricing().length;
        if(length==1)
        {
            Pricing[] pricings = nameList[position].getPricing();
            holder.price1Text.setText(pricings[0].getPrice() + "$");
            holder.unit1Text.setText(pricings[0].getUnit());
        }
        else {
            for (int i = 0; i < length; i++) {
                Pricing[] pricings = nameList[i].getPricing();
                if(i==0) {
                    holder.price1Text.setText(pricings[i].getPrice() + "$");
                    holder.unit1Text.setText(pricings[i].getUnit());
                }
                if(i==1) {
                    holder.price2Text.setText(pricings[i].getPrice() + "$");
                    holder.unit2Text.setText(pricings[i].getUnit());
                }else if(i==2) {
                    holder.price3Text.setText(pricings[i].getPrice() + "$");
                    holder.unit3Text.setText(pricings[i].getUnit());
                }else if(i==3)
                {
                    holder.price4Text.setText(pricings[i].getPrice() + "$");
                    holder.unit4Text.setText(pricings[i].getUnit());
                }
                else if(i==4) {
                    holder.price5Text.setText(pricings[i].getPrice() + "$");
                    holder.unit5Text.setText(pricings[i].getUnit());
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return nameList.length;
    }

}