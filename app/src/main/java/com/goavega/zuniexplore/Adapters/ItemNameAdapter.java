package com.goavega.zuniexplore.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.goavega.zuniexplore.R;
import java.util.List;

public class ItemNameAdapter extends RecyclerView.Adapter<ItemNameAdapter.MyViewHolder> {
    final private List<String> nameList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final public TextView nameText;

        public MyViewHolder(View view) {
            super(view);
            nameText = (TextView) view.findViewById(R.id.itemNameText);

        }
    }

    public ItemNameAdapter(List<String> nameList) {
        this.nameList = nameList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_name_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemNameAdapter.MyViewHolder holder, int position) {
        String name=nameList.get(position);
        holder.nameText.setText(name);

    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
